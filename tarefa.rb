class Usuarios
    #Atributos
    attr_accessor :email, :senha, :nome, :numero, :nascimento, :logado
    
    #Inicializando valores dos atributos
    def initialize(email:, senha:, nome:, numero:, nascimento:, logado:)
        @email = email
        @senha = senha
        @nome = nome
        @numero = numero
        @nascimento = nascimento
        @logado = false
    end
    
    #Método idade
    def idade
        aux = Date.parse(@nascimento)  
        ano = (aux.year).to_i  #para pegar só o ano e transformar para inteiro
        idade = 2020-ano  #Continha para descobrir a idade do usuário
        puts idade
    end

    #Método logar
    def logar(senhainformada)
        if (senhainformada == @senha)  #condicional para verificar se a senha informada é a certa ou não
            @logado = true;
            puts "Senha correta! Você foi logado!"
        end
        else
            puts "Senha incorreta! Login não realizado."
        end
    end

    #Método deslogar
    def deslogar
        @logado = false;  #Caso o usuário deslogue, @logado recebe o valor false
        puts "Você foi deslogado!"
    end

end


class Aluno
    #Atributos
    attr_accessor :matricula, :periodo, :curso, :turmas

    #Inicializando valores dos atributos
    def initialize(matricula:, periodo:, curso:, turmas:)
        @matricula = matricula
        @periodo = periodo
        @curso = curso
        @turmas = []
    end

    #Método inscrever
    def inscrever(nome_turma)
        @turmas.append(nome_turma) #adiciona ao array @turmas o nome da turma na qual o aluno foi inscrito
    end

end


class Turma
    #Atributos
    attr_accessor :nome, :horario, :diasdasemana, :inscritos, :inscricao_abera;

    #Inicializando valores dos atributos
    def initialize(nome:, horario:, diasdasemana:, inscritos:, inscricao_abera:)
        @nome = nome
        @horario = horario
        @diasdasemana = []
        @inscritos = []
        @inscricao_aberta = false
    end

    #Método abrir_inscricao
    def abrir_inscricao
        @inscricao_aberta = true  #a variável @inscricao_aberta recebe true apenas quando as inscrições em turmas estiverem abertas
        puts "Inscrição aberta!"
    end

    #Método fechar_inscricao
    def fechar_inscricao
        @inscricao_aberta = false #a variável @inscricao_aberta recebe false apenas quando as inscrições em turmas estiverem fechadas
        puts "Inscrição fechada!"  
    end

    #Método adicionar_aluno
    def adicionar_aluno(nome_aluno)
        @inscritos.append(nome_aluno); #adiciona nome do aluno ao array @inscritos
    end
end


class Professor
    #Atributos
    attr_accessor :matricula, :salario, :materias;

    #Inicializando valores dos atributos
    def initialize(matricula:, salario:, materias:)
        @matricula = matricula
        @salario = salario.to_f
        @materias = []
    end

    #Método adicionar_materia
    def  adicionar_materia(nome_materia)
        @materias.append(nome_materia)  #adiciona ao array @materias a matéria que o professor irá lecionar
        puts "Matéria adicionada com sucesso!"
    end
end


class materias
    #Atributos
    attr_accessor :ementa, :nome, :professores

    #Inicializando valores dos atributos
    def initialize(ementa:, nome:, professores:)
        @ementa = ementa
        @nome = nome
        @professores = []
    end

    #Método adicionar_professor
    def adicionar_professor(nome_professor)
        @professores.append(nome_professor) #adiciona ao array de professores o nome do professor que lecionará determinada materia
        puts "Professor adicionado!"
    end
end

